GET /product/_search
{
    "query": {
        "match_all": {}
    }
}

GET /product/_search?explain
{
    "query": {
        "term": {
            "name": "lobster"
        }
    }
}