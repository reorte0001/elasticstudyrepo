#!/usr/bin/env python3

import getpass
import datetime
from netmiko import ConnectHandler

IP = str(input('Please enter the devices\'s IP: '))
user = input('Enter your username: ')
password = getpass.getpass('Please enter your password: ')

# Loop para la definicion de parametros de conexion a los dispositivos

accSWs = {
    'device_type': 'cisco_ios',
    'ip': IP,
    'username': user,
    'password': password,
    'secret': password,
    'verbose': True,
}

# Conexion a los dispositivos via SSH
device_connect = ConnectHandler(**accSWs)
print('Entering enable mode...')
device_connect.enable()

# Comandos de configuracion
ise_config = [
    '! Configura el nombre del dispositivo'.upper(),
    'hostname accSW-{}'.format(IP),
    '! Habilitar el soporte de autenticacion web estandar en el switch'.upper(),
    'ip classless',
    'ip http server',
    'ip http secure-server',
    '! Crea usuario para pruebas'.upper(),
    'username test-user password 0 Cisco#2019@',
    'aaa new-model',
    '! Crea un metodo de autenticacion de puertos basada en 802.1X'.upper(),
    'aaa authentication dot1x default group radius',
    '! Requerido para asignacion de VLAN/ACL'.upper(),
    'aaa authorization network default group radius',
    '! Autenticacion y Autorizacion para transacciones webauth'.upper(),
    'aaa authorization auth-proxy default group radius',
    '! Habilita Accounting para 802.1X y MAB'.upper(),
    'aaa accounting dot1x default start-stop group radius',
    'aaa session-id common',
    '! Actualiza la informacion de Accounting periodicamente cada 5 minutos'.upper(),
    'aaa accounting update periodic 5',
    'aaa accounting system default start-stop radius',
    '! Habilitacion de Cambio de Autorizacion (CoA)'.upper(),
    'aaa server radius dynamic-author',
    'client 10.1.1.245 server-key Cisco#2019@',
    '! Incluir el atributo 6 de RADIUS en cada autenticacion'.upper(),
    'radius-server attribute 6 on-for-login-auth',
    '! Incluye el atributo 8 de RADIUS en cada solicitud de acceso'.upper(),
    'radius-server attribute 8 include-in-access-req',
    '! Incluye el atributo 25 de RADIUS en cada solicitud de acceso'.upper(),
    'radius-server attribute 25 access-request include',
    '! Espera 3 x 30 segundos antes de marcar al servidor RADIUS como muerto'.upper(),
    'radius-server dead-criteria time 30 tries 3',
    '! Configuracion del servidor RADIUS utilizando puertos RFC-Standard 1812 y 1813'.upper(),
    'radius server ISE',
    'address ipv4 10.1.1.245 auth-port 1812 acct-port 1813',
    'key Cisco#2019@',
    '! Envia los atributos especificos del vendor'.upper(),
    'radius-server vsa send accounting',
    'radius-server vsa send authentication',
    '! Establece desde que interfaz se originan los paquetes RADIUS'.upper(),
    'ip radius source-interface vlan 123',
    '! Habilita el rastreo de dispositivos'.upper(),
    'ip dhcp snooping',
    'ip device tracking',
    '! Habilita la autenticacion 802.1X en los puertos de switch globalmente'.upper(),
    'dot1x system-auth-control',
    '! Habilita EAP para autenticacion critica (Inaccesible Authentication Bypass)'.upper(),
    'dot1x critical eapol',
    '! Configura un tiempo de delay para Autenticacion Critica'.upper(),
    'authentication critical recovery delay 1000',
    '! Definir la lista de acceso por defecto en el switch'.upper(),
    'ip access-list extended ACL-ALLOW',
    'permit ip any any',
    'ip access-list extended ACL-DEFAULT',
    'remark DHCP'.upper(),
    'permit udp any eq bootpc any eq bootps',
    'remark DNS'.upper(),
    'permit udp any any eq domain',
    'remark Ping'.upper(),
    'permit icmp any any',
    'remark PXE / TFTP'.upper(),
    'permit udp any any eq tftp',
    'remark Permite HTTP/S hacia ISE para Webauth'.upper(),
    'permit tcp any host 10.1.1.245 eq www',
    'permit tcp any host 10.1.1.245 eq 443',
    'permit tcp any host 10.1.1.245 eq 8443',
    'remark Denegar todo el resto'.upper(),
    'deny ip any any',
    '! Lista de acceso para permitir URL-Redirection para Webauth'.upper(),
    'ip access-list extended ACL-WEBAUTH-REDIRECT',
    'deny ip any host 10.1.1.245',
    'permit ip any any',
    '! Configuracion de 802.1x en interfaces'.upper(),
    'interface range gi 1/0-3',
    '! Estable al puerto en acceso para hosts'.upper(),
    'switchport host',
    '! Configura la vlan de datos por defecto'.upper(),
    'switchport access vlan 100',
    '! Configura la vlan de voz por defecto'.upper(),
    'switchport voice vlan 101',
    '! Habilita el modo Monitor de autenticacion'.upper(),
    'authentication open',
    '! Configuracion de ACL debe para agregar dACLs desde el servidor AAA'.upper(),
    'ip access-group ACL-ALLOW in',
    '! Permite multiples endpoints (voz y datos) en un mismo puerto de acceso'.upper(),
    'authentication host-mode multi-auth',
    '! Habilitar re autenticacion'.upper(),
    'authentication periodic',
    '! Habilita la re autenticacion a traves de RADIUS Session-Timeout'.upper(),
    'authentication timer reauthenticate server',
    'authentication event fail action next-method',
    'authentication event server dead action authorize vlan 100',
    'authentication event server alive action reinitialize',
    '! La autenticacion debe ser 802.1X y despues MAB'.upper(),
    'authentication order dot1x mab',
    'authentication priority dot1x mab',
    '! Habilita la autenticacion de puerto en la interfaz'.upper(),
    'authentication port-control auto',
    'authentication violation restrict',
    '! Habilita mab en la interfaz'.upper(),
    'mab',
    '! Habilita autenticacion 802.1X en la interfaz'.upper(),
    'dot1x pae authenticator',
    '! Habilita el periodo de retransmision a 10 segundos'.upper(),
    'dot1x timeout tx-period 10',
    '! Creacion de VLANs y SVIs para el los estados de eforcement'.upper(),
    'vlan 100',
    'name ACCESO',
    'vlan 101',
    'name VOZ',
    '! Configuracion de logging'.upper(),
    'logging monitor informational',
    'logging origin-id ip',
    'logging source-interface vlan 1',
    'logging host 10.1.1.3 transport udp port 5543',
    'logging buffered',
    'logging persistent url flash0:/syslog size 134217728 filesize 16384',
    'logging trap 6',
    'logging buffered 6',
    'logging buffered 16384 6',
    '! Configuracion de control de cambios de configuracion'.upper(),
    'archive',
    'log config',
    'logging enable',
    'logging size 200',
    'hidekeys',
    'notify syslog',
    '! Configuracion de SNMP'.upper(),
    'snmp-server community READONLY RO'
]

# Sentencia de envio de comandos a los dispositivos
output = device_connect.send_config_set(ise_config)
# Impresion de la configuracion aplicada
print(output)

now = datetime.datetime.now()

with open('config-' + str(now), 'w') as config:
    for lines in output:
        config.write(lines)


device_connect.disconnect()



